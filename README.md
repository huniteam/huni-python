# huni-python

In times gone by, this was a set of scripts to create a deb package with a
custom build of python 2, as well as all the dependencies for the huni harvest
and transformation pipeline.

It has now been converted to a docker image.

## Links

* https://bitbucket.org/huniteam/huni-python
* https://hub.docker.com/r/huniteam/huni-python/
