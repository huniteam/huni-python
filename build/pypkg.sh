#!/bin/bash -e

[[ $(whoami) != 'root' ]] && echo "Run this script as root. Exiting now." && exit 0

DEST="/usr/local/huni-python2"
VERSION="2.7.12"

PKGS="distribute beaker psycopg2 pysqlite velruse sqlalchemy \
waitress gunicorn pyramid pyramid_beaker pyramid_debugtoolbar Paste \
pyramid_simpleform pyramid_tm python-ldap webhelpers lxml nose nose-cov \
nose-progressive rednose nose_fixes python-magic requests pyyaml pillow \
networkx python-novaclient pytz httplib2"

retry() {
    count=$1
    shift

    for i in {1..$count}; do
        "$@" && break
    done
}

build_python() {
    wget http://www.python.org/ftp/python/${VERSION}/Python-${VERSION}.tgz
    tar -zxvf Python-${VERSION}.tgz
    cd Python-${VERSION}/
    ./configure --prefix=${DEST}
    make
    make install
}

clean_python() {
    rm -rf ${DEST}/*
    rm -rf Python-${VERSION}
}

install_the_installer() {
#    curl -Ls http://python-distribute.org/distribute_setup.py | ${DEST}/bin/python
    curl -Ls https://bootstrap.pypa.io/get-pip.py | ${DEST}/bin/python
}

install_deps() {
    DEBIAN_FRONTEND=noninteractive apt-get install -y build-essential devscripts curl \
        postgresql-server-dev-9.1 libldap2-dev libsasl2-dev libxslt-dev libxml2-dev \
        libjpeg-dev libfreetype6-dev liblcms2-dev libyaml-dev libncurses5-dev \
        libssl-dev libsqlite3-dev libbz2-dev libsasl2-dev wget
}

setup_base() {
    mkdir -p $DEST
    chown ${USER}. $DEST
}

install_the_pkgs() {
    for pkg in $PKGS ; do
        ${DEST}/bin/pip install $pkg
    done
}

upgrade_the_pkgs() {
    for pkg in $PKGS ; do
        ${DEST}/bin/pip install --upgrade $pkg
    done
}

build_the_pkg() {
    debuild --no-lintian -uc -us -b
}

usage() {

cat <<EOF

    One of:
     - build: remove the existing build env (if exists) and build a new
     python and distribute / pip base.

     - clean: remove the existing build env (if exists) and the local
     instance of the Python tarball

     - install: install the pkgs

     - upgrade: upgrade the pkgs

     - pkg: build the debian pkg

EOF
}
case $1 in

  "build")
    retry 5 install_deps
    setup_base
    build_python
    install_the_installer
    ;;

  "clean")
    setup_base
    clean_python
    ;;

  "install")
    install_the_pkgs
    ;;

  "upgrade")
    upgrade_the_pkgs
    ;;

  "pkg")
    build_the_pkg
    ;;

  *)
    usage
    ;;

esac
